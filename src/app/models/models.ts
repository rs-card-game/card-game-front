export interface Card {
  color: string;
  value: string;
}


export interface CardGameDetails {
  hand: Card[];
  orderedColors: string[];
  orderedValues: string[];
}
