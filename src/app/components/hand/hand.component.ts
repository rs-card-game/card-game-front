import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Card, CardGameDetails} from "../../models/models";
import {CardService} from "../../services/card.service";
import {SortService} from "../../services/sort.service";


@Component({
  selector: 'app-hand',
  templateUrl: './hand.component.html',
  styleUrls: ['./hand.component.css']
})
export class HandComponent implements OnInit {
  //sort = new EventEmitter<string>();
  @Output() generatedHandEvent = new EventEmitter<Card[]>();
  cardGameDetails!: CardGameDetails;
  sortedHand!: Card[];
  newHand = false;

  constructor(private cardService: CardService,
              private sortService: SortService
  ) {
  }


  ngOnInit(): void {
  }

  generateHand(): void {
    this.cardService.generateHand().subscribe({
      next: this.handleGeneratedResponse.bind(this)
    });
  }

  sortHand(): void {
    this.sortService.sortHand(this.cardGameDetails.hand).subscribe({
      next: this.handleSortResponse.bind(this)
    });
  }

  handleGeneratedResponse(cardGameDetails: CardGameDetails) {
    this.cardGameDetails = cardGameDetails;
    this.generatedHandEvent.emit(cardGameDetails.hand);
    this.newHand = true;
    console.log('hands', cardGameDetails);
  }


  handleSortResponse(hand: Card[]) {
    console.log('sortedHand', hand);
    this.sortedHand = hand
    this.newHand = false;
  }


}
