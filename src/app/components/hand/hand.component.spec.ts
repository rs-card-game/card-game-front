import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HandComponent} from './hand.component';
import {CardService} from '../../services/card.service';
import {SortService} from '../../services/sort.service';
import {of} from 'rxjs';
import {Card, CardGameDetails} from '../../models/models';

describe('HandComponent', () => {
  let component: HandComponent;
  let fixture: ComponentFixture<HandComponent>;
  let cardServiceSpy: jasmine.SpyObj<CardService>;
  let sortServiceSpy: jasmine.SpyObj<SortService>;

  beforeEach(async () => {
    const cardServiceSpyObj = jasmine.createSpyObj('CardService', ['generateHand']);
    const sortServiceSpyObj = jasmine.createSpyObj('SortService', ['sortHand']);

    await TestBed.configureTestingModule({
      declarations: [HandComponent],
      providers: [
        {provide: CardService, useValue: cardServiceSpyObj},
        {provide: SortService, useValue: sortServiceSpyObj}
      ]
    })
      .compileComponents();

    cardServiceSpy = TestBed.inject(CardService) as jasmine.SpyObj<CardService>;
    sortServiceSpy = TestBed.inject(SortService) as jasmine.SpyObj<SortService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate hand', () => {
    const mockCards: Card[] = [
      {color: 'Heart', value: 'Ace'},
      {color: 'Spade', value: 'King'},
      {color: 'Diamond', value: 'Queen'},
      {color: 'Club', value: 'Jack'},
      {color: 'Heart', value: '10'},
      {color: 'Spade', value: '9'},
      {color: 'Diamond', value: '8'},
      {color: 'Club', value: '7'},
      {color: 'Heart', value: '6'},
      {color: 'Spade', value: '5'}
    ];

    const cardGameDetails: CardGameDetails = {
      hand: mockCards,
      orderedColors: ['red', 'black'],
      orderedValues: ['A', '2', '3']
    };
    ;
    cardServiceSpy.generateHand.and.returnValue(of(cardGameDetails));

    const emitSpy = spyOn(component.generatedHandEvent, 'emit');
    component.generateHand();

    expect(cardServiceSpy.generateHand).toHaveBeenCalled();
    expect(emitSpy).toHaveBeenCalledWith(cardGameDetails.hand);
    expect(component.newHand).toBeTrue();
  });

  it('should sort hand', () => {
    const mockHand: Card[] = [/* mock cards */];
    sortServiceSpy.sortHand.and.returnValue(of(mockHand));

    component.cardGameDetails = {hand: mockHand} as CardGameDetails;
    component.sortHand();

    expect(sortServiceSpy.sortHand).toHaveBeenCalledWith(mockHand);
    expect(component.sortedHand).toEqual(mockHand);
    expect(component.newHand).toBeFalse();
  });
});
