import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {MatSnackBar} from '@angular/material/snack-bar';
import {of, throwError} from 'rxjs';
import {Card} from '../models/models';
import {SortService} from './sort.service';

describe('SortService', () => {
  let service: SortService;
  let httpMock: HttpTestingController;
  let snackBarSpy: jasmine.SpyObj<MatSnackBar>;

  beforeEach(() => {
    const snackBarSpyObj = jasmine.createSpyObj('MatSnackBar', ['open']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        SortService,
        {provide: MatSnackBar, useValue: snackBarSpyObj}
      ]
    });
    service = TestBed.inject(SortService);
    httpMock = TestBed.inject(HttpTestingController);
    snackBarSpy = TestBed.inject(MatSnackBar) as jasmine.SpyObj<MatSnackBar>;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort the hand', () => {
    const mockHand: Card[] = [
      {color: 'red', value: 'A'},
      {color: 'black', value: '2'},
      {color: 'red', value: '3'}
    ];
    const sortedHand: Card[] = [
      {color: 'black', value: '2'},
      {color: 'red', value: '3'},
      {color: 'red', value: 'A'}
    ];

    service.sortHand(mockHand).subscribe(response => {
      expect(response).toEqual(sortedHand);
    });

    const req = httpMock.expectOne('http://localhost:8080/hand/sort');
    expect(req.request.method).toBe('PUT');
    req.flush(sortedHand);
  });

  it('should handle HTTP errors', () => {
    const mockHand: Card[] = [
      {color: 'red', value: 'A'},
      {color: 'black', value: '2'},
      {color: 'red', value: '3'}
    ];
    const errorMessage = 'Internal Server Error';

    service.sortHand(mockHand).subscribe(
      () => fail('should have failed with the error message'),
      error => {
        expect(error).toBe('Something went wrong. Please try again later.');
        expect(snackBarSpy.open).toHaveBeenCalledWith('Something went wrong. Please try again later.', 'Close', {
          duration: 2000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
          panelClass: ['red-snackbar']
        });
      }
    );

    const req = httpMock.expectOne('http://localhost:8080/hand/sort');
    req.error(new ErrorEvent('error'), {status: 500, statusText: errorMessage});
  });


});
