import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, Observable, throwError} from 'rxjs';
import {Card} from "../models/models";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class SortService {

  private readonly API_URL = 'http://localhost:8080/hand/sort';

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {
  }

  sortHand(hand: Card[]): Observable<Card[]> {
    return this.http.put<Card[]>(this.API_URL, hand).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('An error occurred in sortHand:', error);
        this.openErrorToast('Something went wrong. Please try again later.');
        return throwError(() => 'Something went wrong. Please try again later.');
      }));
  }

  private openErrorToast(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
      panelClass: ['red-snackbar'],
    });
  }
}
