import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CardGameDetails} from '../models/models';
import {CardService} from './card.service';

describe('CardService', () => {
  let service: CardService;
  let httpMock: HttpTestingController;
  let snackBarSpy: jasmine.SpyObj<MatSnackBar>;

  beforeEach(() => {
    const snackBarSpyObj = jasmine.createSpyObj('MatSnackBar', ['open']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CardService,
        {provide: MatSnackBar, useValue: snackBarSpyObj}
      ]
    });
    service = TestBed.inject(CardService);
    httpMock = TestBed.inject(HttpTestingController);
    snackBarSpy = TestBed.inject(MatSnackBar) as jasmine.SpyObj<MatSnackBar>;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should generate a hand', () => {
    const mockResponse: CardGameDetails = {
      hand: [
        {color: 'red', value: 'A'},
        {color: 'black', value: '2'},
        {color: 'red', value: '3'}
      ],
      orderedColors: ['red', 'black'],
      orderedValues: ['A', '2', '3']
    };

    service.generateHand().subscribe(response => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpMock.expectOne('http://localhost:8080/hand');
    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });

  it('should handle HTTP errors', () => {
    const errorMessage = 'Internal Server Error';

    service.generateHand().subscribe(
      () => fail('should have failed with the error message'),
      error => {
        expect(error).toBe('Something went wrong. Please try again later.');
        expect(snackBarSpy.open).toHaveBeenCalledWith('Something went wrong. Please try again later.', 'Close', {
          duration: 2000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
          panelClass: ['red-snackbar']
        });
      }
    );

    const req = httpMock.expectOne('http://localhost:8080/hand');
    req.error(new ErrorEvent('error'), {status: 500, statusText: errorMessage});
  });

});
